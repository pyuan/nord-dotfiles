"   vimplug plugins
call plug#begin(expand('~/.config/nvim/plugged'))
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'arcticicestudio/nord-vim',
Plug 'mattn/emmet-vim',
Plug 'itchyny/lightline.vim',
Plug 'tpope/vim-fugitive',
Plug 'terryma/vim-multiple-cursors',
Plug 'ap/vim-css-color',
call plug#end()

"   activate syntax colorscheme
syntax on
colorscheme nord

"	activate .rasi syntax highlighting
au BufNewFile,BufRead /*.rasi setf css

"   set activator key for html plugin
let g:user_emmet_leader_key=','

"	stop automatic comments
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
autocmd FileType html setlocal ts=2 sts=2 sw=2
autocmd FileType css setlocal ts=2 sts=2 sw=2
autocmd FileType javascript setlocal ts=2 sts=2 sw=2

"   set text width and tab spaces
set tw=79 ts=4 sts=0 sw=4 "expandtab add this to python in the future

"   enable custom vimrc for file extensions
filetype plugin on

"   enable line numbers
set relativenumber

"   lightline and uses vim-fugitive for git status
set laststatus=2
let g:lightline = {
      \ 'colorscheme': 'nord',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'fugitive#head',
      \   'filename': 'LightLineFilename'
      \ },
      \ }
function! LightLineFilename()
  return expand('%:p')
endfunction
let g:deoplete#enable_at_startup = 10
inoremap <silent><expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"
inoremap <silent><expr><s-tab> pumvisible() ? "\<c-p>" : "\<s-tab>"
autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif

" python keymaps
map <F5> :w\|!python3 % <cr>
map <F4> :w\|!python3 -m doctest % -v <cr>
